import { createRouter,createWebHistory } from 'vue-router'

function page(filename: string) {
  return () => import(`../pages/${filename}.vue`);
}

const routes = [
  { path: '/', name:'main', component: page('Main') },
  { path: '/game', name:'game', component: page('Game') },
  { path: '/info', name:'info', component: page('Info') },
]

export default createRouter({
  routes,
  history: createWebHistory(),
  linkActiveClass: "active",
  linkExactActiveClass: "exact-active",
})