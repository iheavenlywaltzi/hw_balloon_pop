export const atlases = {
  blue: '/images/atlases/balloon_blue.json',
  green: '/images/atlases/balloon_green.json',
  pink: '/images/atlases/balloon_pink.json',
  violet: '/images/atlases/balloon_violet.json',
  yellow: '/images/atlases/balloon_yellow.json',
}
