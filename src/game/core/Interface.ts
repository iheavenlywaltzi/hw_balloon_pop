import { Sprite } from "pixijs"

export type IOnScoreChangeCallback = (score: number) => void
// export interface IOnScoreChangeCallback { (score: number): void }

export interface IGameObject {
  sprite: Sprite  
}

export interface IMoveGameObject{ 
  sprite: Sprite 
  isAlive:boolean
  type:string
  move(delta:number):void
  configure():void
  draw():void
}

export interface IOnClick<T = undefined> {
  onClick(data: T):void
}

export type IResurceLoadData = {[key: string]: any};
