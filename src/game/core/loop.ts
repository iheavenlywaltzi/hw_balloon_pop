import { Ticker } from "pixijs"
import { GAME_CONFIGS } from "../configs"
import { GeneratorBalloon } from "../generators/generatorBalloon"
import { IMoveGameObject } from "./Interface"
import Scene from "./scene"

export default class Loop {
  #ticker!:Ticker
  #generator!:GeneratorBalloon
  #scene:Scene
  #simpleIter: number = 0
  #bonusIter: number = 0

  constructor(scene:Scene,generatorBalloon:GeneratorBalloon){
    this.#scene = scene
    this.#generator = generatorBalloon
    this.#ticker = new Ticker()
    // this.#ticker.maxFPS = 60 // Хотябы на будущее для оптимизации ,и возможности вынести в настройки
    this.#ticker.add((delta)=>{this.#render(delta)})
  }

  start(){ this.#ticker.start() }

  stop(){ this.#ticker.stop() }

  #render(delta:number){
    const list = this.#scene.getMoveListObject()
    list.forEach(element => {
      element.move(delta)
      if(!element.isAlive){
        // да тут можно и switch как в генераторе,
        // но вызывать несколько методов, мне лично в таких случиях удобно через if
        // Естественно мне несоставит труда сделать так как скажут, или как нужно будет по критериям компании
        if(element.type === 'simple'){
          element.configure()
          element.draw()
        }else if(element.type === 'bonus'){
          this.#controllerBonusBallon(delta,element)
        }
        // если шаров и, их видов станет вдруг очень много, то естественно тут разрастётся выборка
        // в таком случии  ( зная что их может быть очень много) я бы перенёс в отдельный метод
        // Или вынес бы этот функционал в сам шарик, ( но соглосовав данное действие с выше стоящим)
      }
    });
    
    if(list.length < GAME_CONFIGS.maxBalloonSum)
      this.#controllerCreateSimpleBallon(delta)
  }

  #controllerCreateSimpleBallon(delta:number){
    if(this.#simpleIter >= GAME_CONFIGS.balloonCreationTime){
      this.#scene.addMoveObject(this.#generator.newBalloon())
      this.#simpleIter = 0
    } else {
      this.#simpleIter+= delta
    }
  }

  #controllerBonusBallon(delta:number,element:IMoveGameObject){
    if(this.#bonusIter >= GAME_CONFIGS.bonusBalloonCreationTime){
      element.configure()
      element.draw()
      this.#bonusIter = 0
    } else {
      this.#bonusIter+= delta
    }
  }
}