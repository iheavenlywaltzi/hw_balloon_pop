import { Cache, Assets } from 'pixijs'
import { IResurceLoadData } from './Interface'

type IResursePack = { [key: string]: string }
type IOnLoadSuccess = (data: IResurceLoadData) => void

export default class AssetsLoader {
  #paths: IResursePack = {}
  #onLoadSuccess:IOnLoadSuccess = ()=>{}

  static Builder = class {
    #loader: AssetsLoader = new AssetsLoader()

    setPaths(paths:IResursePack): this {
      this.#loader.#paths = paths
      return this
    }

    setOnLoadSuccessCB(onLoadSuccess:IOnLoadSuccess): this {
      this.#loader.#onLoadSuccess = onLoadSuccess
      return this
    }

    build():AssetsLoader{
      return this.#loader
    }
  }

  load() {
    const cachedData = this.#loadFromCache()

    if(cachedData) { this.#onLoadSuccess(cachedData) }
    else { this.#loadResurce() }
  }

  #loadFromCache(): IResurceLoadData | undefined {
    return Object.keys(this.#paths).reduce((acc: IResurceLoadData | undefined, key)=>{
      const cachedData = Cache.get(key)
      if (!acc || !cachedData) return;
      acc[key] = cachedData
      return acc;
    }, {})
  }

  #loadResurce(){
    Cache.reset()
    Assets.addBundle('ballons', this.#paths);
    Assets.loadBundle('ballons').then(()=>{
      const cachedData = this.#loadFromCache()
      if (!cachedData) {
        console.error("loadResurce Errors")
        return 
      }
      this.#onLoadSuccess(cachedData)
    })
  }
}
