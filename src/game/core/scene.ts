import { Application, IApplicationOptions, Sprite, Texture } from 'pixijs'
import { GeneratorBackground } from '../generators/generatorBackground';
import { IGameObject, IMoveGameObject } from './Interface';

export default class Scene {
  #container:HTMLElement
  #pixiApp!:Application
  listMoveObject:IMoveGameObject[] = []

  constructor(container:HTMLElement){
    this.#container = container
  }

  create(pixiConfigs:IApplicationOptions | undefined){
    if(!this.#pixiApp)
      this.#pixiApp = new Application(pixiConfigs);

    this.#container.appendChild(this.#pixiApp.view as HTMLCanvasElement);
    
    this.addObject(new GeneratorBackground(pixiConfigs))
  }

  addObject(object:IGameObject){
    this.#pixiApp.stage.addChild(object.sprite);
  }

  addMoveObject(object: IMoveGameObject){
    this.#pixiApp.stage.addChild(object.sprite);
    this.listMoveObject.push(object)
  }

  getMoveListObject(){
    return this.listMoveObject
  }

  getApp(){
    return this.#pixiApp
  }
}
