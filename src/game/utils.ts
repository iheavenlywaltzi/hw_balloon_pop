export const getRandomInt = (min: number,max: number)=>{
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

export const getRandomFloat = (min: number, max: number)=>{
  return Math.random() * (max - min) + min;
}
