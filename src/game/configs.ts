export const GAME_CONFIGS = {
  maxBalloonSum: 28, // максимальное количество шаров 
  balloonCreationTime: 100, // время через которое появляются новые ОБЫЧНЫЕ шарики
  bonusBalloonCreationTime: 500, // время через которое появляются БОНУСНЫЕ шарики
}

export const PIXI_APP_CONFIGS = {
  width: 480, //window.innerWidth,
  height: window.innerHeight, //window.innerHeight,
  antialias: true,
  backgroundColor: '#FF8564'
}
