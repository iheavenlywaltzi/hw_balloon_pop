import { AnimatedSprite, Sprite } from "pixijs";
import { IGameObject, IOnClick } from "../core/Interface";

export abstract class BaseBalloon implements IGameObject {
  abstract sprite: AnimatedSprite
  abstract type: string

  get isAlive(): boolean {
    return this.sprite.interactive
  }
  set isAlive(val: boolean) {
    this.sprite.interactive = val
  }

  speed: number = 1
  points: number = 1
  hitArea: number[] = [
    0,45,
    13,20,
    40,5,
    55,5,
    80,20,
    95,45,
    95,75,
    77,107,
    55,125,
    40,125,
    20,110,
    0,75,
  ]

  setOnClick(handler: IOnClick<number>): void {
    this.sprite.on('pointerdown', ()=>{
      this.sprite.play()
      handler.onClick(this.points)
    });
    this.sprite.onComplete = () => {
      this.#death()
    }
  }

  move(delta:number){
    this.isAlive && (this.sprite.y-= delta*this.speed);
    this.goBeyond()
  }

  goBeyond(){
    (this.sprite.y < -200) && (this.#death());
  }

  configure(){}
  draw(){}

  #death(){
    this.isAlive = false
    this.sprite.alpha = 0
    this.sprite.scale.set(0)
  }
}
