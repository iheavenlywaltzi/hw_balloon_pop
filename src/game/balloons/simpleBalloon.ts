import { AnimatedSprite, Polygon, Resource, Texture } from 'pixijs'
import { getRandomInt, getRandomFloat } from '../utils'
import { BaseBalloon } from './abstractBalloon'

export default class SimpleBalloon extends BaseBalloon {
  sprite: AnimatedSprite
  speed: number = 1
  points: number = 1
  width: number
  height: number
  type!: string

  constructor(sprite:AnimatedSprite,width:number,height:number) {
    super()
    this.sprite = sprite
    this.width = width
    this.height = height
    this.type = 'simple'
  }

  draw(){
    this.isAlive = true
  }

  configure(texture?:Texture<Resource>){
    this.speed = getRandomFloat(0.5, 1.5)
    this.points = getRandomInt(1, 6)

    if(texture) this.sprite.texture = texture
    this.sprite.x = getRandomInt(40, this.width-120)
    this.sprite.y = getRandomInt(this.height+100, this.height)
    this.sprite.anchor.set(0.0)
    this.sprite.hitArea = new Polygon(this.hitArea);
    this.sprite.scale.set(getRandomFloat(0.5, 1.5),getRandomFloat(0.5, 1.5))
    this.sprite.animationSpeed = getRandomFloat(0.8, 1.2)
    this.sprite.loop = false
    this.sprite.alpha = 1
    this.sprite.currentFrame = 0
    this.sprite.onFrameChange = (inf:number) => {
      this.sprite.alpha = (1-(inf / 10))
    };
  }
}
