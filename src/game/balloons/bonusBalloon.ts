import { AnimatedSprite, Polygon, Resource, Texture } from 'pixijs'
import { getRandomInt, getRandomFloat } from '../utils'
import SimpleBalloon from './simpleBalloon'

export default class BonusBalloon extends SimpleBalloon {
  #tintStepTimer:number = 0

  constructor(sprite:AnimatedSprite,width:number,height:number) {
    super(sprite,width,height)
    this.type = 'bonus'
  }

  move(delta:number){
    this.isAlive && (this.sprite.y-= delta*this.speed)
    if(this.#tintStepTimer >= 10) { // да можно 10 вынести даже в глобальные настройки, если потребуется
      this.sprite.tint = Math.random() * 0xFFFFFF
      this.#tintStepTimer = 0
    }
    this.#tintStepTimer += delta
    this.goBeyond()
  }
  
  configure(texture?:Texture<Resource>){
    this.speed = getRandomFloat(1.5, 2.5)
    this.points = 100

    if(texture) this.sprite.texture = texture
    this.sprite.x = getRandomInt(40, this.width-120)
    this.sprite.y = this.height+200
    this.sprite.anchor.set(0.0)
    this.sprite.scale.set(1)
    this.sprite.hitArea = new Polygon(this.hitArea);
    this.sprite.animationSpeed = 1
    this.sprite.loop = false
    this.sprite.alpha = 1
    this.sprite.currentFrame = 0
    this.sprite.tint = Math.random() * 0xFFFFFF
    this.sprite.onFrameChange = (inf:number) => {
      this.sprite.alpha = (1-(inf / 10))
    };
  }
}
