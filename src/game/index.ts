import * as CONF from './configs'
import * as RESS from './resources'

import { IOnScoreChangeCallback, IResurceLoadData } from './core/Interface';
import AssetsLoader from './core/assetsLoader';
import Scene from './core/scene';
import Loop from './core/loop';
import { GeneratorBalloon } from './generators/generatorBalloon';


class Game {
  #containerSelector: string
  #scene!: Scene
  #loop!: Loop
  #status: string = 'stop'
  #onScoreChange: IOnScoreChangeCallback = ()=>{}

  constructor(containerSelector:string = 'body'){
    this.#containerSelector = containerSelector
  }

  async init(containerSelector:string = this.#containerSelector): Promise<string> {
    let state = 'ok'
    const containerBlock = document.querySelector(containerSelector);
    if (!containerBlock) throw "No block"

    /* 
      * Load all resources 
    */
    const resourcesData: IResurceLoadData = await new Promise((resolve)=>{
      new AssetsLoader.Builder().setPaths(RESS.atlases).setOnLoadSuccessCB((data)=>{
        resolve(data)
      }).build().load()
    })

    /* 
      * Create Scene
    */
    this.#scene = new Scene(containerBlock as HTMLElement)
    this.#scene.create(CONF.PIXI_APP_CONFIGS)
    // const pixiApp = this.#scene.getApp()

    /* 
      * Init generator
    */
    const generatorBalloon = new GeneratorBalloon(resourcesData,this.#onScoreChange)

    /* 
      * Loop render
    */
    this.#loop = new Loop(this.#scene,generatorBalloon);

    // add start balloons
    this.#scene.addMoveObject(generatorBalloon.newBalloon())
    this.#scene.addMoveObject(generatorBalloon.newBalloon('bonus'))

    return state;
  }

  play(){
    if(this.#status === 'play') return
    this.#status = 'play'
    this.#loop.start()
  }

  pause(){
    // for the future ;)
  }

  stop(){
    if(this.#status === 'stop') return
    this.#status = 'stop';
    this.#loop.stop()
  }

  onScoreChange(callback:IOnScoreChangeCallback){
    this.#onScoreChange = callback
  }  
}
export default Game;
