import { AnimatedSprite } from "pixijs";
import { BaseBalloon } from "../balloons/abstractBalloon";
import BonusBalloon from "../balloons/bonusBalloon";
import SimpleBalloon from "../balloons/simpleBalloon";
import { PIXI_APP_CONFIGS } from "../configs";
import { IOnScoreChangeCallback, IResurceLoadData } from "../core/Interface";
import { getRandomInt } from "../utils";

export class GeneratorBalloon{
  #resourcesSpriteData:IResurceLoadData
  #OnClick:IOnScoreChangeCallback

  constructor(resourcesData:IResurceLoadData,OnClick:IOnScoreChangeCallback){
    this.#resourcesSpriteData = resourcesData
    this.#OnClick = OnClick
  }

  newBalloon(type:string='simple'){
    const keysSpriteData = Object.keys(this.#resourcesSpriteData)
    const randomKeySprite = keysSpriteData[getRandomInt(0,keysSpriteData.length)]
    const anim = new AnimatedSprite(this.#resourcesSpriteData[randomKeySprite].animations['burst']);
    let balloonObject:BaseBalloon

    switch (type) {
      case 'simple':
        balloonObject = new SimpleBalloon(anim,PIXI_APP_CONFIGS.width,PIXI_APP_CONFIGS.height)
        break;
    
      case 'bonus':
        balloonObject = new BonusBalloon(anim,PIXI_APP_CONFIGS.width,PIXI_APP_CONFIGS.height)
        break;
    
      default:
        balloonObject = new SimpleBalloon(anim,PIXI_APP_CONFIGS.width,PIXI_APP_CONFIGS.height)
        break;
    }

    if (!balloonObject) throw "No create Balloon"

    balloonObject.configure()
    balloonObject.setOnClick({ onClick:this.#OnClick })
    balloonObject.draw()
    return balloonObject
  }



}