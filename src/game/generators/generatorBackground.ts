import { IApplicationOptions, Sprite, Texture } from "pixijs";
import { IGameObject } from "../core/Interface";

export class GeneratorBackground implements IGameObject {
  sprite: Sprite
  #canvas = document.createElement('canvas')
  #width: number
  #height: number
  #quality: number = 10;

  constructor(pixiConfigs:IApplicationOptions | undefined){
    this.#width = pixiConfigs?.width || 100
    this.#height = pixiConfigs?.height || 100
    this.#canvas.width = 1;
    this.#canvas.height = this.#quality;
    
    this.#createLinearGradient()
    
    const gradTexture = Texture.from(this.#canvas);
    this.sprite = this.#createSprite(Texture.from(this.#canvas)) 
  }

  #createSprite(texture:Texture){
    const sprite = new Sprite(texture);
    sprite.position.set(0, 0);
    sprite.rotation = 0;
    sprite.width = this.#width
    sprite.height = this.#height
    return sprite
  }

  #createLinearGradient(){
    const ctx = this.#canvas.getContext('2d');
    if(!ctx) throw "No context"
  
    const linearGradient = ctx.createLinearGradient(0, 0, 0, this.#quality);
      linearGradient.addColorStop(0, '#FFA27B');
      linearGradient.addColorStop(0.2, '#FF5D5A');
      linearGradient.addColorStop(1, '#FF9568');
  
    ctx.fillStyle = linearGradient;
    ctx.fillRect(0, 0, 1, this.#quality);
  }
}
