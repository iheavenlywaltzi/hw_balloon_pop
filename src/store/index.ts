import { createStore } from 'vuex'

const store = createStore({
  state () {
    return {
      count: 0
    }
  },
  mutations:{
    incrementCount(state, payload = 1) {
      state.count += payload;
    },
    resetCount(state) {
      state.count = 0;
    }
  },
  actions: {
    incrementCount({ commit, state }, payload) {
      commit('incrementCount', payload)
      localStorage.setItem('pageCouner', state.count.toString(10));
    },
    resetCount({ commit, state }, payload){
      commit('resetCount')
    }
  }
})

export default store